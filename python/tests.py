import unittest
import main
import switching


class BotTestCase(unittest.TestCase):

    def setUp(self):
        self.bot = main.PahnaBot(key="key", name="testing", socket=None)

    def tearDown(self):
        self.bot = None

    def test_inside_same_piece(self):
        self.set_fields(self.bot, 0, 50, 0, 60)

        distance_moved = self.bot.get_distance_moved()

        self.assertEqual(10, distance_moved)

    def test_from_piece_to_another(self):
        self.set_fields(self.bot, 0, 90, 1, 10, {0: 100, 1: 100})

        distance_moved = self.bot.get_distance_moved()

        self.assertEqual(20, distance_moved)

    def test_length_of_a_lane(self):
        radius = 100
        angle = 45
        left_lane_distance_from_center = -10
        right_lane_distance_from_center = 10

        result = self.bot.get_length_of_a_lane(radius, angle, right_lane_distance_from_center)
        self.assertAlmostEqual(70.68583, result, 5)

        result = self.bot.get_length_of_a_lane(radius, angle, left_lane_distance_from_center)
        self.assertAlmostEqual(86.39380, result, 5)

    def test_find_piece_indices_until_next_switch(self):
        self.bot.game_data = self.create_game_data_with([
            {
                "length": 100.0,
                "switch": True
            },
            {
                "length": 100.0,
            },
            {
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 100.0,
                "switch": True
            }
        ])
        self.bot.create_piece_type_index_lists()

        current_index = 0
        result = switching.find_piece_indices_until_next_switch(current_index, [0, 3], 4)

        self.assertEqual(2, len(result))

    def test_find_piece_indices_until_next_switch_roll_over(self):
        self.bot.game_data = self.create_game_data_with([
            {
                "length": 100.0
            },
            {
                "length": 100.0,
                "switch": True
            },
            {
                "radius": 200,
                "angle": 22.5
            }
        ])
        self.bot.create_piece_type_index_lists()

        current_index = 1
        result = switching.find_piece_indices_until_next_switch(current_index, [1], 3)

        self.assertEqual(2, len(result))

    def test_count_length_for_lane(self):
        piece_indices = [0, 1, 2]
        piece_lengths = {0: 10, 1: [10, 20], 2: 5}

        result = switching.count_length_for_lane(0, piece_indices, piece_lengths)
        self.assertEqual(25, result)

        result = switching.count_length_for_lane(1, piece_indices, piece_lengths)
        self.assertEquals(35, result)

    def set_fields(self, bot, prev_piece_index, prev_dist, current_piece_index, current_dist, piece_lengths=None):
        bot.prev_piece_index = prev_piece_index
        bot.prev_dist = prev_dist
        bot.current_piece_index = current_piece_index
        bot.current_dist = current_dist
        bot.piece_lengths = piece_lengths

    def create_game_data_with(self, pieces):
        return {
            "race": {
                "track": {
                    "pieces": pieces,
                    "lanes": [
                        {
                            "distanceFromCenter": -20,
                            "index": 0
                        },
                        {
                            "distanceFromCenter": 0,
                            "index": 1
                        }
                    ]
                }
            }
        }

if __name__ == '__main__':
    unittest.main()