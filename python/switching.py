import numbers


def resolve_preferred_lanes_per_switch(switch_indices, lane_count, piece_count, piece_lengths):
    result = {}
    for switch_index in switch_indices:
        piece_indices = find_piece_indices_until_next_switch(switch_index, switch_indices, piece_count)

        length_per_lane = []
        for lane_index in range(0, lane_count):
            length_per_lane.append(count_length_for_lane(lane_index, piece_indices, piece_lengths))

        shortest_lane_index = 0
        shortest = length_per_lane[0]
        for i, length in enumerate(length_per_lane):
            if length < shortest:
                shortest_lane_index = i

        result[switch_index] = shortest_lane_index

    return result


def find_piece_indices_until_next_switch(current_index, switch_indices, piece_count):
    index_in_switch_list = switch_indices.index(current_index)

    if is_last_switch(index_in_switch_list, switch_indices):
        next_switch_index = switch_indices[0]
    else:
        next_switch_index = switch_indices[index_in_switch_list + 1]

    first_piece_index = current_index + 1
    if next_switch_index > current_index:
        return range(first_piece_index, next_switch_index)
    else:
        return range(first_piece_index, piece_count) + range(0, next_switch_index)


def is_last_switch(index_in_switch_list, switch_indices):
    return index_in_switch_list + 1 == len(switch_indices)


def count_length_for_lane(lane_index, piece_indices, piece_lengths):
    total = 0
    for piece_index in piece_indices:
        length_or_lengths = piece_lengths[piece_index]
        if isinstance(length_or_lengths, numbers.Number):
            total += length_or_lengths
        else:
            total += length_or_lengths[lane_index]

    return total


def should_switch(current_piece_index, switch_indices, current_lane_index, preferred_lanes):
    """
    Currently tries to switch every time to the shorter lane.
    """
    on_switch = current_piece_index in switch_indices
    on_wrong_lane = False
    if on_switch:
        on_wrong_lane = current_lane_index != preferred_lanes[current_piece_index]

    return on_switch and on_wrong_lane