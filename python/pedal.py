#pedal to the medal


def get_new_throttle(throttle_amount, pieces, piece_lengths, current_piece_index, speed):
    current_piece = pieces[current_piece_index]
    next_piece_index = (current_piece_index + 1) % len(pieces)
    next_piece = pieces[next_piece_index]

    #todo calculate max speed for curves
    #todo adjust speed for next curve
    #todo adjust drifting factor

    if not "radius" in current_piece and not "radius" in next_piece:
        throttle_amount = 0.80
    elif "radius" in current_piece:
        #drift!
        throttle_amount += 0.01
        if throttle_amount > 0.65:
            throttle_amount = 0.65
    elif "radius" in next_piece:
        throttle_amount = 0.65
    else:
        pass
    
    return throttle_amount

