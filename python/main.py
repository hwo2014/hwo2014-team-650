import json
import socket
import sys
import math
import numbers
import pedal
import switching


class PahnaBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.game_data = {}
        self.me = None

        self.turbo_data = None

        self.piece_count = 0
        self.straight_indices = []
        self.switch_indices = []
        self.left_turn_indices = []
        self.right_turn_indices = []

        self.preferred_lane_per_switch = {}

        self.prev_piece_index = 0
        self.current_piece_index = 0

        self.prev_dist = 0
        self.prev_lane = 0
        self.current_dist = 0
        self.distance_moved = 0

        self.prev_tick = 0
        self.current_tick = 0

        self.piece_lengths = {}

        self.throttle_amount = 0.65

        self.speed = 0
        self.angle = 0

    def debug(self):
        print 'piece indx', self.current_piece_index, self.game_data["race"]["track"]["pieces"][self.current_piece_index]
        print('distance: %.3f' % self.current_dist)
        print('delta dist: %.3f' % self.distance_moved)
        print('car angle: %.3f' % self.angle)
        print('speed: %.3f' % self.speed)
        print('throttle: %.2f' % self.throttle_amount)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def create_race_data(self, track_name, car_count, password=None):
        race_data = {"botId": {"name": self.name,
                               "key": self.key},
                     "trackName": track_name,
                     "carCount": car_count}

        if password is not None:
            race_data["password"] = password

        return race_data

    def create_race(self, race_data):
        return self.msg("createRace", race_data)

    def join_race(self, race_data):
            return self.msg("joinRace", race_data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def use_turbo(self):
        self.msg("turbo", "Wohoo!")
        self.turbo_data = None

    def switch_left(self):
        self.msg("switchLane", "Left")

    def switch_right(self):
        self.msg("switchLane", "Right")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        track = "keimola"
        car_count = 1
        password = "laalaa"
        race_data = self.create_race_data(track, car_count, password)
        #self.create_race(race_data)
        #self.join_race(race_data)
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        print data
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        print data
        self.game_data = data
        json_pretty = json.dumps(self.game_data, sort_keys=True, indent=4)
        print json_pretty

        self.create_piece_type_index_lists()

        self.piece_lengths = self.resolve_piece_lengths()
        self.preferred_lane_per_switch = switching.\
            resolve_preferred_lanes_per_switch(self.switch_indices,
                                               len(self.game_data["race"]["track"]["lanes"]),
                                               self.piece_count,
                                               self.piece_lengths)

        print "Preferred lanes per switch:", self.preferred_lane_per_switch

        self.ping()

    def on_game_start(self, data):
        print("Race started")
        print data
        self.ping()

    def on_car_positions(self, car_positions):
        self.me = next(car for car in car_positions if car["id"]["name"] == self.name)
        self.current_piece_index = self.me["piecePosition"]["pieceIndex"]

        self.angle = self.me["angle"]

        if "radius" in self.game_data["race"]["track"]["pieces"][self.me["piecePosition"]["pieceIndex"]]:
            pass
            #print "curve!"
            #self.debug()

        #if self.angle > 0:
        #    self.throttle_amount = 0.0
        #else:
        #    self.throttle_amount = 0.7

        self.update_distance_moved()

        if switching.should_switch(self.current_piece_index,
                                   self.switch_indices,
                                   self.get_current_lane_index(),
                                   self.preferred_lane_per_switch):
            preferred_lane = self.preferred_lane_per_switch[self.current_piece_index]
            current_lane = self.get_current_lane_index()
            if preferred_lane > current_lane:
                #print "Switch right!"
                self.switch_right()
            else:
                #print "Switch left!"
                self.switch_left()

        elif self.should_use_turbo():
            self.use_turbo()

        else:
            new_throttle = self.get_new_throttle()
            if new_throttle != self.throttle_amount or self.speed == 0:
                self.throttle(self.throttle_amount)
                self.throttle_amount = new_throttle
            else:
                self.ping()

    def turbo_available(self, turbo_data):
        self.turbo_data = turbo_data

    def on_crash(self, data):
        print("Someone crashed")
        print data
        self.debug()
        #self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print data
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.turbo_available,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if "gameTick" in msg:
                #print "Tick:", msg['gameTick']
                self.prev_tick = self.current_tick
                self.current_tick = msg['gameTick']
                #self.debug()
            else:
                print "no tick"

            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

    def update_distance_moved(self):
        in_piece_distance = self.me["piecePosition"]["inPieceDistance"]

        """
        if self.game_data['race']['track']['pieces'][self.current_piece_index].has_key("angle"):
            piece = self.game_data['race']['track']['pieces'][self.current_piece_index]
            lane_r = self.game_data['race']['track']['lanes'][self.get_current_lane_index()]["distanceFromCenter"]

            if piece["angle"] > 0:
                #invert if right turn to get correct radius
                lane_r = -lane_r

            r = piece["radius"] + lane_r
            
            distance_percent = in_piece_distance / abs(self.count_bend_length(piece["angle"], piece["radius"]))
            self.current_dist = abs(self.count_bend_length(piece["angle"] * distance_percent, r))
           
            #some sanity checks
            max_bend = abs(self.count_bend_length(piece["angle"] * distance_percent, rad))
            if self.current_dist > max_bend:
                print 'cur dist', self.current_dist, 'greater than max bend', max_bend
                print 'percentage=', distance_percent
            if distance_percent > 1:
                print 'invalid dist_percent', distance_percent
            
        """
            
        self.current_dist = in_piece_distance
        self.distance_moved = self.get_distance_moved()
        
        timediff = self.current_tick - self.prev_tick

        if timediff == 0:
            self.speed = -1
        else:
            self.speed = self.distance_moved / timediff

        self.prev_piece_index = self.current_piece_index
        self.prev_lane = self.get_current_lane_index()
        self.prev_dist = self.current_dist

    def get_distance_moved(self):
        if self.prev_piece_index == self.current_piece_index:
            tmp = self.current_dist - self.prev_dist
            if tmp < 0:
                print 'neggg distance:', tmp
                print 'cur:', self.current_dist, 'prev:', self.prev_dist
            return tmp
        else:
            prev_piece_length = self.piece_lengths[self.prev_piece_index]

            if not isinstance(prev_piece_length, numbers.Number):
                prev_piece_length = prev_piece_length[self.prev_lane]

            tmp = (prev_piece_length - self.prev_dist) + self.current_dist
            if tmp < 0:
                print 'negative dist moved:', tmp
                print 'prev indx:', self.prev_piece_index, 'cur indx:', self.current_piece_index
                print 'prev piece len:', prev_piece_length, 'prev dist:', self.prev_dist, 'cur dist:', self.current_dist
            return tmp

    def get_piece_length(self, piece, index):
        if "length" in piece:
            return piece["length"]
        elif "angle" in piece:
            lanes = self.game_data["race"]["track"]["lanes"]
            lane_lengths = []
            for lane in lanes:
                lane_length = self.get_length_of_a_lane(piece["radius"], piece["angle"], lane["distanceFromCenter"])
                lane_lengths.append(abs(lane_length))

            print lane_lengths
            return lane_lengths
        else:
            raise Exception("Could not determine length for piece: " + index)

    @staticmethod
    def count_bend_length(angle, radius):
        return angle * (math.pi / 180) * radius

    def get_length_of_a_lane(self, radius, angle, lane_distance_from_center):
        if angle > 0:
            #right turn radius needs to be negated to get correct radius
            lane_distance_from_center = -lane_distance_from_center
        return self.count_bend_length(angle, radius + lane_distance_from_center)

    def resolve_piece_lengths(self):
        pieces = self.game_data["race"]["track"]["pieces"]

        piece_lengths = {}
        for i, piece in enumerate(pieces):
            piece_lengths[i] = self.get_piece_length(piece, i)
            print i, 'len:', piece_lengths[i]

        return piece_lengths

    def create_piece_type_index_lists(self):
        track = self.game_data["race"]["track"]
        pieces = track["pieces"]
        self.piece_count = len(pieces)

        for i, piece in enumerate(pieces):
            if "switch" in piece and piece["switch"]:
                self.switch_indices.append(i)

            elif "angle" in piece:
                if piece["angle"] < 0:
                    self.left_turn_indices.append(i)
                else:
                    self.right_turn_indices.append(i)
            else:
                self.straight_indices.append(i)

    @staticmethod
    def find_turn_index(switch_index, indices):
        try:
            return next(i for i in indices if i > switch_index)
        except StopIteration:
            #TODO stupid?
            return -1

    def get_new_throttle(self):
        return pedal.get_new_throttle(self.throttle_amount,
                                      self.game_data['race']['track']['pieces'],
                                      self.piece_lengths,
                                      self.current_piece_index,
                                      self.speed)

    def should_use_turbo(self):
        return self.turbo_data is not None and self.is_final_stretch()

    def is_final_stretch(self):
        return (self.game_data["race"]["raceSession"]["laps"] - 1) == self.me["piecePosition"]["lap"] and\
            self.rest_is_straight()

    def rest_is_straight(self):
        for i in range(self.current_piece_index, self.piece_count):
            if not self.is_straight_or_switch(i):
                return False

        return True

    def is_straight_or_switch(self, piece_index):
        return piece_index in self.straight_indices or piece_index in self.switch_indices

    def get_current_lane_index(self):
        return self.me["piecePosition"]["lane"]["startLaneIndex"]

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = PahnaBot(s, name, key)
        bot.run()
